let source = document.getElementById("blog");
let openNav = document.getElementById("openNav");
let closeNav = document.getElementById("closeNav");
let addElementBtn = document.getElementsByClassName("add-element");
let copyBtn = document.getElementById("copyHTML");
let mainContainer = document.getElementById("blog");

openNav.addEventListener("click", function () {
    document.getElementById("mySidenav").style.width = "450px";
    document.getElementById("main").style.marginLeft = "450px";
})

closeNav.addEventListener("click", function () {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
})

for (var i = 0; i < addElementBtn.length; i++) {
    addElementBtn[i].addEventListener('click', fetchData, false);
}

function generateId() {
    return (Math.random().toString(16) + "000000000").substr(2, 8);
}

function fetchData() {
    var attribute = this.getAttribute('htype');
    if (attribute == 'img-text') {
        let img = document.getElementById("itxt-img");
        let text = document.getElementById("itxt-text");
        addElement({
            type: 'image-text',
            img: img.value,
            text: text.value
        });
        img.value = '';
        text.value = '';
    }
    if (attribute == 'text-img') {
        let img = document.getElementById("txti-img");
        let text = document.getElementById("txti-text");
        addElement({
            type: 'text-image',
            img: img.value,
            text: text.value
        });
        img.value = '';
        text.value = '';
    }
    if (attribute == 'title') {
        let text = document.getElementById("title-text");
        addElement({
            type: 'title',
            text: text.value
        });
        text.value = '';
    }
    if (attribute == 'image') {
        let img = document.getElementById("img-url");
        addElement({
            type: 'single-image',
            img: img.value
        });
        img.value = '';
    }
    if (attribute == 'only-text') {
        let text = document.getElementById("only-text");
        addElement({
            type: 'only-text',
            text: text.value
        });
        text.value = '';
    }
}

copyBtn.addEventListener("click", function () {
    copyHTML();
})

function addElement({
    type,
    text,
    img
} = {}) {
    let mainContainer = document.getElementById("blog");
    let uid = generateId();
    switch (type) {
        case 'single-image':
            console.log('Single image');
            mainContainer.innerHTML += `<div id=${uid}><img class="blog-full-img" src="${img}"><button uid="${uid}" class="deleteBtn" onClick="deleteElement('${uid}')">Usuń</button></div>`
            break;
        case 'text-image':
            console.log('text-image')
            mainContainer.innerHTML += `<div class="blog-container" id=${uid}>
                <div class="blog-text">${text}</div>
                <img class="blog-img" src="${img}">
                <button uid="${uid}" class="deleteBtn" onClick="deleteElement('${uid}')">Usuń</button>
                </div>`
            break;
        case 'image-text':
            console.log('image-text')
            mainContainer.innerHTML += `<div class="blog-container" id=${uid}>
                <img class="blog-img" src="${img}">
                <div class="blog-text">${text}</div>
                <button uid="${uid}" class="deleteBtn" onClick="deleteElement('${uid}')">Usuń</button>
                </div>`
            break;
        case 'title':
            console.log('title');
            mainContainer.innerHTML += `<p class="blog-section" id=${uid}>${text} <button uid="${uid}" class="deleteBtn" onClick="deleteElement('${uid}')">Usuń</button></p>`
            break;
        case 'only-text':
            console.log('only-text');
            mainContainer.innerHTML += `<div class="blog-text-full" id=${uid}>${text} <button uid="${uid}" class="deleteBtn" onClick="deleteElement('${uid}')">Usuń</button></div>`
            break;
        default:
            console.log('no option selected')
    }
}

function deleteElement(elementId) {
    console.log('element id', elementId)
    let element = document.getElementById(elementId);
    element.remove();
}

function copyHTML() {
    let copyTarget = document.getElementById("blog").innerHTML;
    let htmlString = copyTarget.toString();
    console.log(htmlString);
    let regex = /(<button uid=.+?<\/button>)/g
    let result = htmlString.replace(regex, '');
    copyStringToClipboard(result);
}

function copyStringToClipboard(str) {
    var el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style = {
        position: 'absolute',
        left: '-9999px'
    };
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}